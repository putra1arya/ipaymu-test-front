import '@babel/polyfill'
import 'mutationobserver-shim'
import Vue from 'vue'
import './plugins/axios'
import './plugins/bootstrap-vue'
import App from './App.vue'
import store from './store'
import axios from './plugins/axios'
import vuetify from './plugins/vuetify'

Vue.config.productionTip = false

new Vue({
  store,
  axios,
  vuetify,
  render: h => h(App)
}).$mount('#app')
