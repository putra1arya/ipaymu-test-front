
export default {
    namespaced: true,
    state : {
        status : false,
        updata : {
            nama : '',
            pekerjaan : '',
            tanggal_lahir : '',
            keterangan : '',
            id : ''
        }
    },
    getters : {
        status : (state) => {
            return state.status
        },
        updata : (state) => state.updata
    },
    mutations : {
        setStatus : (state,payload) => {
            state.status = payload.status
            state.updata = payload.updata
        },
    },
    actions : {
        setStatus : ({commit}, payload) =>{
            commit('setStatus', payload)
        },

    }
}
